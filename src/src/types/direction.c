/*
 * direction.c
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#include "direction.h"

char* direction_tostring(direction_t dir)
{
	switch(dir)
	{
	case DIR_UP: return "UP";
	case DIR_DOWN: return "DOWN";
	case DIR_NODIR: return "NODIR";
	case DIR_ANY: return "ANY";
	default: return "#INVALID DIR#";
	}
	return "#INVALID DIR#"; // Won't really hit this line anyways.
}
