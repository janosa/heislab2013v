/*
 * elevatormanager.c
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#include <unistd.h>
#include "elevatormanager.h"
#include "heislab.h"

void elevatormanager_init()
{

}

void elevatormanager_set_speed(int value)
{
	if(value > 0)
		g_state.current_dir = DIR_UP;
	else if(value == 0)
	{
		// We are stopping, but we have to actively break the engine, by setting the engine to reverse speed for a short period of time.
		switch(g_state.current_dir)
		{
		case DIR_UP:
			elev_set_speed(-100); // Set hastighet nedover
			break;
		case DIR_DOWN:
			elev_set_speed(100); // Set hastighet oppover
			break;
		default:
			elev_set_speed(value);
			g_state.current_dir = DIR_NODIR;
			return;
		}
		usleep(40000);

		g_state.current_dir = DIR_NODIR;
	}
	else
	{
		g_state.current_dir = DIR_DOWN;
	}
	elev_set_speed(value);
}

int elevatormanager_get_stop_signal()
{
	return elev_get_stop_signal();
}

int elevatormanager_get_obstruction_signal()
{
	return elev_get_obstruction_signal();
}

int elevatormanager_get_floor_sensor_signal(floor_t* floor)
{
	if(floor == NULL)
		return 0;
	if(elev_get_floor_sensor_signal() == floor->floor_no)
		return 1;
	return 0;
}

floor_t* elevatormanager_get_active_floor_signal()
{
	int floor_no = elev_get_floor_sensor_signal();
	if(floor_no < 0 || floor_no >= FLOOR_COUNT)
		return NULL;
	int i;
	for(i = 0; i < FLOOR_COUNT; i++)
	{
		if(g_floors[i].floor_no == floor_no)
			return &g_floors[i];
	}
	return NULL;
}

int elevatormanager_is_between_floors()
{
	if(elev_get_floor_sensor_signal() == -1)
		return 1;
	return 0;
}

int elevatormanager_get_button_signal(floor_t* floor, direction_t dir)
{
	if(floor == NULL)
		return 0;
	switch (dir) {
	case DIR_UP:
		return elev_get_button_signal(BUTTON_CALL_UP, floor->floor_no);
	case DIR_DOWN:
		return elev_get_button_signal(BUTTON_CALL_DOWN, floor->floor_no);
	case DIR_NODIR:
		return elev_get_button_signal(BUTTON_COMMAND, floor->floor_no);
	default:
		return 0;
	}
	return 0;
}

void elevatormanager_set_door_state(int value)
{
	g_state.door_open = value;
}
