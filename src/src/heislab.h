/*
 * heislab.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef HEISLAB_H_
#define HEISLAB_H_

#include "config.h"

#include "api/elev.h"


#include "types/floor.h"
#include "types/direction.h"
#include "types/order.h"

#include "types/state.h"

#include "orderlist.h"
#include "ordermanager.h"
#include "lightmanager.h"
#include "elevatormanager.h"
#include "elevator.h"

extern floor_t g_floors[FLOOR_COUNT];
extern state_t g_state;

#endif /* HEISLAB_H_ */
