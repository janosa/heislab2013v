/*
 * floor.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef FLOOR_H_
#define FLOOR_H_

#include <string.h>

typedef struct
{
	char name[30];
	int floor_no;
} floor_t;

void floor_set_name(floor_t* floor, char* name);

#endif /* FLOOR_H_ */
