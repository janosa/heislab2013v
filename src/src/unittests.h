/*
 * unittests.h
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#ifndef UNITTESTS_H_
#define UNITTESTS_H_

int unittest_init();
void unittest_orderlist();
void unittest_lights();
void unittest_elevator();




#endif /* UNITTESTS_H_ */
