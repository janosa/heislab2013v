/*
 * elevator.c
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#include "elevator.h"
#include "heislab.h"
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>


state_t g_state;
floor_t g_floors[FLOOR_COUNT];
pthread_t g_thread_io;
pthread_t g_thread_statemachine;
int g_thread_io_run;
int g_thread_statemachine_run;


void elevator_init()
{
	// Sett default verdier

	// Deklarer de forskjellige etasjene
	// Ingen mulighet for navnsetting enda
	int i;
	for(i = 0; i < FLOOR_COUNT; i++)
	{
		floor_t* pFloor = &g_floors[i];
		pFloor->floor_no = i;
		sprintf(pFloor->name, "%d. ETASJE", i+1);
	}

	orderlist_init();

	g_state.last_floor = &g_floors[0];
	g_state.current_dir = DIR_NODIR;
	g_state.target_dir = DIR_NODIR;
	g_state.target_floor = &g_floors[0];

	g_state.stop_activated = 0;
	g_state.door_open = 0;

	// Initialiser submoduler
	orderlist_init();
	ordermanager_init();
	lightmanager_init();
	elevatormanager_init();

	return;
}

void elevator_start()
{
	// Initialiser heisen til en kjent tilstand.
	elevator_reset();

	g_thread_io_run = 1;
	pthread_create(&g_thread_io, NULL, &elevator_thread_io, NULL);
	// Start thread on main thread
	g_thread_statemachine_run = 1;
	pthread_create(&g_thread_statemachine, NULL, &elevator_thread_statemachine, NULL);
}

void* elevator_thread_io(void* arg)
{
	while(g_thread_io_run)
	{
		// Behandle input
		elevator_handle_stop();
		elevator_handle_obstruction();
		ordermanager_fetch_orders();

#ifdef ENABLE_QF_FORCE_DOOR
		// Quickfix for closing doors if not in floor. Tydeligvis en studass som har lyst til å fysisk dra vekk heisen fra en etasje
		if(g_state.door_open && elevatormanager_is_between_floors())
		{
			g_state.door_open = 0; // Close door
		}
#endif

		// Få lysene til å representere systemets tilstand
		lightmanager_update_stop_indicator();
		lightmanager_update_door_indicator();
		lightmanager_update_floor_indicators();
		lightmanager_update_order_lamps();
		// Vent 10ms. Ingen grunn til å bruke mye CPU på å kjøre dette for fort, da noen neppe trykker på en knapp i mindre enn 10ms.
		usleep(10000);
	}
	return NULL;
}

void* elevator_thread_statemachine(void* arg)
{
	// Start hovedløkken
	while(g_thread_statemachine_run)
	{
		// Vent på bestilling
		while(orderlist_count() == 0 && g_thread_statemachine_run)
			usleep(10000);

		if(!g_thread_statemachine_run)
			break; // Hopp ut av løkken hvis tilstandsmaskinen skal avslutte

		// Kalkuler hvilken bestilling vi skal ta, og styr heisen mot etasjen
		ordermanager_calculate_target_floor();
		switch(g_state.target_dir)
		{
		case DIR_UP:
			elevatormanager_set_speed(SPEED_UP);
			break;
		case DIR_DOWN:
			elevatormanager_set_speed(SPEED_DOWN);
			break;
		default:
			elevatormanager_set_speed(SPEED_STOP);
			break;
		}

		if(g_state.current_dir == DIR_NODIR && elevatormanager_is_between_floors())
		{
			// Vi beveger oss ingen vei og vi er mellom en etasje. Dette er en dead lock.
			// Vi fikser dette med å gå nedover til nærmeste etasje.
			elevatormanager_set_speed(SPEED_DOWN);
			while(elevatormanager_is_between_floors() && g_thread_statemachine_run);
			if(!g_thread_statemachine_run)
				break; // Hopp ut av løkken hvis tilstandsmaskinen skal avslutte

			elevatormanager_set_speed(SPEED_STOP);
		}

		usleep(500000); // Vent 500ms for at heisen skal komme seg vekk fra sensoren
		while(elevatormanager_is_between_floors() && g_thread_statemachine_run);
		if(!g_thread_statemachine_run)
			break; // Hopp ut av løkken hvis tilstandsmaskinen skal avslutte


		floor_t* cur_floor = elevatormanager_get_active_floor_signal();
		if(cur_floor != NULL)
		{
			if(ordermanager_should_stop_on_floor(cur_floor, g_state.target_dir))
			{
				// Det finnes en bestilling til etasjen vi er i nå.
				elevatormanager_set_speed(SPEED_STOP);
				ordermanager_on_stopped_on_floor(cur_floor, g_state.target_dir);
				// Kalkuler hvor vi skal neste
				ordermanager_calculate_target_floor();
#if VERBOSE >= 1
				printf("Reached floor %s Opening door. Going %s\n", cur_floor->name, direction_tostring(g_state.target_dir));
#endif
				elevator_load_passenger(); // Åpne døren og vent til passasjeren(e) har kommet inn/ut
			}
		}

		usleep(10000);
	}
	return NULL;
}

void elevator_reset()
{
	orderlist_clear();
	// Vent på at obstruksjon og stopp skal gå bort
	while(elevatormanager_get_obstruction_signal() || elevatormanager_get_stop_signal());

	// Set heis til å gå nedover
	elevatormanager_set_speed(SPEED_DOWN);
	// Vent på at heisen når første etasje
	while(!elevatormanager_get_floor_sensor_signal(&g_floors[0]));
	elevatormanager_set_speed(SPEED_STOP);
	return;
}

void elevator_handle_stop()
{
	if(elevatormanager_get_stop_signal())
	{
		g_state.stop_activated = 1;
		// Clear orders
		orderlist_clear();
		g_thread_statemachine_run = 0;
		pthread_cancel(g_thread_statemachine); // Stopp  tilstandsmaskinen
		elevatormanager_set_speed(SPEED_STOP);
		// Reset relaterte tilstandsvariable til default verdier
		g_state.target_dir = DIR_NODIR;
		g_state.target_floor = g_state.last_floor;

#ifdef ENABLE_QF_STOP
		// Quick fix open doors when stop in floor
		if(!elevatormanager_is_between_floors())
		{
			g_state.door_open = 1;
		}
#endif
	}
}

void elevator_handle_obstruction()
{
	if(elev_get_obstruction_signal())
	{
		// Hvis døren er åpen og vi er i en etasje blir dette behandlet allerede
		if(g_state.door_open && !elevatormanager_is_between_floors())
			return;

		// Skru av tilstandsmaskinen
		g_thread_statemachine_run = 0;
		pthread_cancel(g_thread_statemachine);
		elevatormanager_set_speed(SPEED_STOP); // Stopp heisen
	}
	else if(g_thread_statemachine_run == 0 && !g_state.stop_activated)
	{
		// Vi har tidligere hatt en obstruksjon som ikke lengre er der. Forsett hva vi gjorde før
		g_thread_statemachine_run = 1;
		pthread_create(&g_thread_statemachine, NULL, &elevator_thread_statemachine, NULL);
	}
}

void elevator_load_passenger()
{
	elevatormanager_set_door_state(1); // Åpne døren
	usleep(DOOR_OPEN_TIME_MS*1000); // La den stå åpen i x antall millisekund
	while(elevatormanager_get_obstruction_signal()) usleep(DOOR_OPEN_TIME_MS*1000); // Ikke lukk døren så lenge det er noe i veien
	elevatormanager_set_door_state(0); // Lukk døren
}

void elevator_reinit_after_stop()
{
	g_state.stop_activated = 0; // Deaktiver stopp funksjon
	// Restart state machine
	g_thread_statemachine_run = 1;
	pthread_create(&g_thread_statemachine, NULL, &elevator_thread_statemachine, NULL);
}

void elevator_destroy()
{
	g_thread_io_run = 0;
	g_thread_statemachine_run = 0;

	pthread_cancel(g_thread_io); // Stop io tråden
	pthread_cancel(g_thread_statemachine); // Stopp tilstandsmaskinen
	elevatormanager_set_speed(SPEED_STOP);

	// Skru av lys
	lightmanager_set_stop_lamp(0);
	lightmanager_set_floor_indicator(&g_floors[0]);
	lightmanager_set_door_open_lamp(0);

	int i;
	for(i = 0; i < FLOOR_COUNT; i++){
		lightmanager_set_button_lamp(&g_floors[i], DIR_NODIR, 0);
	}
	for(i = 0; i < FLOOR_COUNT-1; i++){
		lightmanager_set_button_lamp(&g_floors[i], DIR_UP, 0);
	}
	for(i = 1; i < FLOOR_COUNT; i++){
		lightmanager_set_button_lamp(&g_floors[i], DIR_DOWN, 0);
	}

}
