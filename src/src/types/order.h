/*
 * order.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef ORDER_H_
#define ORDER_H_

#include "floor.h"
#include "direction.h"

typedef struct
{
	int active; // For å utnytte statisk minne, benytter vi av en indikator om objektet finnes eller ikke
	floor_t* floor;
	direction_t direction;
} order_t;


#endif /* ORDER_H_ */
