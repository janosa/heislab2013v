/*
 * orderlist.c
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#include "heislab.h"
#include <stdio.h>

#define ORDERLIST_LENGTH FLOOR_COUNT*4
order_t g_orders[ORDERLIST_LENGTH];
order_t* g_orderlist_head;
order_t* g_orderlist_tail;

void orderlist_init() {
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		pOrder->active = 0;
		pOrder->floor = 0;
		pOrder->direction = DIR_NODIR;
	}
	g_orderlist_head = &g_orders[0];
	g_orderlist_tail = &g_orders[ORDERLIST_LENGTH-1];
}

order_t* orderlist_get(floor_t* floor, direction_t dir) {
	if (floor == NULL)
		return 0;
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (!pOrder->active)
			continue;
		if (pOrder->floor == floor) {
			if (dir != DIR_ANY && pOrder->direction != dir)
				continue;
			return pOrder;
		}
	}

	return 0;
}

int orderlist_contains(floor_t* floor, direction_t dir) {
	if (orderlist_get(floor, dir))
		return 1;
	return 0;
}

int orderlist_append(floor_t* floor, direction_t dir) {
	if (floor == NULL || dir == DIR_ANY)
		return 0;

	order_t* pOrder = orderlist_get(floor, dir);
	if (pOrder) // Bestilling finnes allerede. Ikke skap duplikater
	{
		return 1;
	}

	int i; // Find first inactive element
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (pOrder->active)
			continue;
		pOrder->active = 1;
		pOrder->floor = floor;
		pOrder->direction = dir;
#if VERBOSE >= 2
		printf("Added an order to %s with direction %s\n", floor->name, direction_tostring(dir));
#endif
		return 1;
	}
	return 0;
}

int orderlist_remove(floor_t* floor, direction_t dir) {
	if (floor == NULL)
		return 0;
	int removedSomething = 0;
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (!pOrder->active)
			continue;
		if (pOrder->floor == floor) {
			if (dir != DIR_ANY && pOrder->direction != dir)
				continue;
			pOrder->active = 0;
			removedSomething++;
#if VERBOSE >= 2
			printf("Removed an order to %s with direction %s\n", floor->name, direction_tostring(pOrder->direction));
#endif
		}
	}
	return removedSomething;
}

order_t* orderlist_first()
{
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (pOrder->active)
			return pOrder;
	}
	return 0;
}

order_t* orderlist_last()
{
	int i;
	for (i = ORDERLIST_LENGTH-1; i >= 0; i--) {
		order_t* pOrder = &g_orders[i];
		if (pOrder->active)
			return pOrder;
	}
	return 0;
}

order_t* orderlist_next(order_t* cur)
{
	if(cur == 0) // invalid pointer
		return 0;
	if(cur < g_orderlist_head || cur > g_orderlist_tail)
		return 0; // Invalid pointer
	cur++; // Get next element in list
	while(cur <= g_orderlist_tail)
	{
		if(cur->active)
			return cur;
		cur++; // Get next element in list
	}
	return 0;
}

int orderlist_clear()
{
	int count = 0;
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (pOrder->active)
		{
			pOrder->active = 0;
			count++;
		}
	}
	return count;
}

int orderlist_count()
{
	int count = 0;
	int i;
	for (i = 0; i < ORDERLIST_LENGTH; i++) {
		order_t* pOrder = &g_orders[i];
		if (pOrder->active)
		{
			count++;
		}
	}
	return count;
}

int orderlist_has_orders_in_dir(floor_t* floor, direction_t dir)
{
	if(dir == DIR_UP)
	{
		int i;
		for(i = 0; i < ORDERLIST_LENGTH; i++)
		{
			if(!g_orders[i].active)
				continue;
			if(g_orders[i].floor->floor_no > floor->floor_no)
				return 1;
		}
	}
	else if(dir == DIR_DOWN)
	{
		int i;
		for(i = 0; i < ORDERLIST_LENGTH; i++)
		{
			if(!g_orders[i].active)
				continue;
			if(g_orders[i].floor->floor_no < floor->floor_no)
				return 1;
		}
	}
	return 0;
}
