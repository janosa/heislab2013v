/*
 * elevatormanager.h
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#ifndef ELEVATORMANAGER_H_
#define ELEVATORMANAGER_H_

#include "types/floor.h"
#include "types/direction.h"

#define SPEED_UP 300
#define SPEED_STOP 0
#define SPEED_DOWN -300

/**
 * Initialiser heiskontrolleren
 */
void elevatormanager_init();

/**
 * Sett hastigheten til heisen.
 * Verdier mellom -300 og +300 gir fornuftige hastigheter.
 * Positive verdier gir enn oppover bevegelse
 */
void elevatormanager_set_speed(int value);

/**
 * Sjekker om stopp knappen er trykket inn.
 */
int elevatormanager_get_stop_signal();

/**
 * Sjekker om noe står i veien for dørene
 */
int elevatormanager_get_obstruction_signal();

/**
 * Åpne eller lukke døren
 * 1 = åpen, 0 = lukket
 */
void elevatormanager_set_door_state(int value);

/**
 * Sjekker om etasjesensoren i en etasje detekterer heisen.
 */
int elevatormanager_get_floor_sensor_signal(floor_t* floor);

/**
 * Returnerer hvilken etasjesensor som er aktiv, eller NULL hvis man ikke er i en etasje.
 */
floor_t* elevatormanager_get_active_floor_signal();

/**
 * Sjekker om at ingen av etasjesensorene er aktivert, dvs i mellom to etasjer
 */
int elevatormanager_is_between_floors();

/**
 * Sjekker om en etasjeknapp i heisen eller en av bestillingsknappene i heisen er trykket inn.
 * Dir argumentet brukes som følger:
 *
 * dir = DIR_NODIR => Etasjeknapp for etasje [floor] inni heisen
 * dir = DIR_UP => Knapp opp i etasje [floor]
 * dir = DIR_DOWN => Knapp ned i etasje [floor]
 */
int elevatormanager_get_button_signal(floor_t* floor, direction_t dir);


#endif /* ELEVATORMANAGER_H_ */
