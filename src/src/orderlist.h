/*
 * orderlist.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef ORDERLIST_H_
#define ORDERLIST_H_

#include "types/order.h"

/**
 * Setter opp ordlisten med default verdier.
 */
void orderlist_init();

/**
 * Henter et element i listen. Støtter dir == DIR_ANY som wildcards i søket.
 * Hvis det finnes mer enn et element som matcher filteret vil resultatet være udefinert.
 * Den vil returnere en av elementene som matcher, men det er ingen prioritet definert.
 */
order_t* orderlist_get(floor_t* floor, direction_t dir);

/**
 * Forteller om et element eksisterer i listen. Returnerer et tall forskjellig fra 0 hvis det finnes.
 * Støtter dir == DIR_ANY som wildcards i søket.
 */
int orderlist_contains(floor_t* floor, direction_t dir);

/**
 * Legger til et nytt element i listen. Det skal ikke kunne finnes to elementer med samme etasje og retning.
 * Hvis det allerede finnes et element i listen med samme etasje og retning vil det ikke legges inn duplikater.
 *
 * Returnerer et tall forskjellig fra 0 hvis elementet ble lagt til/oppdatert
 */
int orderlist_append(floor_t* floor, direction_t dir);


/**
 * Sletter et element i listen. Støtter dir == DIR_ANY som wildcards i søket.
 * Hvis det finnes mer enn et element som matcher filteret vil alle elementene bli slettet.
 *
 * Returnerer hvor mange elementer som ble slettet.
 */
int orderlist_remove(floor_t* floor, direction_t dir);

/**
 * Henter den første gyldige bestillingen i listen eller null hvis den er tom.
 */
order_t* orderlist_first();

/**
 * Henter den siste gyldige bestillingen i listen eller null hvis den er tom.
 */
order_t* orderlist_last();

/**
 * Henter den neste gyldige bestillingen i listen eller null hvis det ikke er flere gyldige bestillinger.
 */
order_t* orderlist_next(order_t* cur);

/**
 * Tøm listen for alle bestillinger.
 * Returnerer hvor mange bestillinger som ble slettet
 */
int orderlist_clear();

/**
 * Returnerer hvor mange bestillinger det er i listen.
 */
int orderlist_count();

/**
 * Sjekker om det finnes bestillinger som er over/eller under (basert på dir) etasjen.
 */
int orderlist_has_orders_in_dir(floor_t* floor, direction_t dir);

#endif /* ORDERLIST_H_ */
