/*
 * lightmanager.h
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#ifndef LIGHTMANAGER_H_
#define LIGHTMANAGER_H_

#include "types/floor.h"
#include "types/direction.h"

/**
 * Initialiserer lyskontrolleren
 */
void lightmanager_init();

/**
 * Sett hvilken etasje lysindikatoren skal vise.
 */
void lightmanager_set_floor_indicator(floor_t* floor);

/**
 * Skru av eller på lyset rundt en etasjeknapp inni heisen, eller bestillingsknappene utenfor heisen.
 *
 * dir = DIR_NODIR => Etasjeknapp for etasje [floor] inni heisen
 * dir = DIR_UP => Knapp opp i etasje [floor]
 * dir = DIR_DOWN => Knapp ned i etasje [floor]
 */
void lightmanager_set_button_lamp(floor_t* floor, direction_t dir, int value);

/**
 * Skru av eller på dør åpen lampen
 */
void lightmanager_set_door_open_lamp(int value);

/**
 * Skru av eller på stop lampen.
 */
void lightmanager_set_stop_lamp(int value);

/**
 * Oppdaterer lysene til bestillingsknappene.
 */
void lightmanager_update_order_lamps();

/**
 * Oppdatere etasjeindikatorlysene
 */
void lightmanager_update_floor_indicators();

/**
 * Oppdatere stopplyset.
 */
void lightmanager_update_stop_indicator();

/**
 * Oppdatere dørlys
 */
void lightmanager_update_door_indicator();

#endif /* LIGHTMANAGER_H_ */
