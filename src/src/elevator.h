/*
 * elevator.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef ELEVATOR_H_
#define ELEVATOR_H_

/**
 * Initialiserer modulen og alle submoduler
 */
void elevator_init();

/**
 * Starter selve heisprogrammet.
 * Denne funksjonen vil aldri returnere
 */
void elevator_start();

// "Private members"

/**
 * Dette er entrypointet til IO tråden.
 * Oppgaven er å samle inn nye bestillinger, behandle stopp og obstruksjoner samt styre lys
 */
void* elevator_thread_io(void* arg);


/**
 * Dette er entrypointet til tilstandsmaskinen.
 * Den tar seg av styringen av heisen.
 */
void* elevator_thread_statemachine(void* arg);

/**
 * Reseter hele systemet til en kjent tilstand. Greit for initialisering og feilbehandling
 *
 * Prosedyren er som følger:
 * Slett alle bestillinger
 * Vent på at obstruksjonssensoren ikke er trykt inn og at stopp knappen ikke er trykket inn
 * Steng dør
 * Kjør ned til første etasje
 * Stopp
 */
void elevator_reset();

/**
 * Behandle hva som skjer når stoppknappen blir trykket på.
 * Skal:
 * Slette alle bestillinger
 * Stoppe heisen.
 * Bare tillate nye bestillinger fra inni heisen.
 */
void elevator_handle_stop();

/**
 * Behandle hva som skjer hvis en obstruksjon skjer. Også midtveis mellom etasjer. (Burde VIRKELIG ikke skje)
 */
void elevator_handle_obstruction();

/**
 * Åpner døren og lar en passasjer (eller flere) tre inn.
 */
void elevator_load_passenger();

/**
 * Funksjon som tar seg av gjennoppstarten etter et nødstop
 */
void elevator_reinit_after_stop();

/**
 * Skru av systemet på en "finere" måte
 */
void elevator_destroy();

#endif /* ELEVATOR_H_ */
