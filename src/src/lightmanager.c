/*
 * lightmanager.c
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#include "lightmanager.h"
#include "heislab.h"


void lightmanager_init()
{

}

void lightmanager_set_floor_indicator(floor_t* floor)
{
	if(floor == NULL)
		return;
	elev_set_floor_indicator(floor->floor_no);
}

void lightmanager_set_button_lamp(floor_t* floor, direction_t dir, int value)
{
	if(floor == NULL)
		return;
	switch (dir) {
	case DIR_UP:
		elev_set_button_lamp(BUTTON_CALL_UP, floor->floor_no, value);
		break;
	case DIR_DOWN:
		elev_set_button_lamp(BUTTON_CALL_DOWN, floor->floor_no, value);
		break;
	case DIR_NODIR:
		elev_set_button_lamp(BUTTON_COMMAND, floor->floor_no, value);
		break;
	default:
		break;
	}
	return;

}

void lightmanager_set_door_open_lamp(int value)
{
	elev_set_door_open_lamp(value);
}

void lightmanager_set_stop_lamp(int value)
{
	elev_set_stop_lamp(value);
}

void lightmanager_update_order_lamps()
{
	int i;
	for(i = 0; i < FLOOR_COUNT; i++)
	{
		// Oppdater bestillingslys inni heisen
		if(orderlist_contains(&g_floors[i], DIR_NODIR))
		{
			// Skru på lys inni heisen
			lightmanager_set_button_lamp(&g_floors[i], DIR_NODIR, 1);
		}
		else
		{
			lightmanager_set_button_lamp(&g_floors[i], DIR_NODIR, 0);
		}

		// Oppdater bestillinglys oppover utenfor heisen
		if(i < FLOOR_COUNT-1)
		{
			if(orderlist_contains(&g_floors[i], DIR_UP))
			{
				// Skru på lys på opp knappene utenfor heisen
				lightmanager_set_button_lamp(&g_floors[i], DIR_UP, 1);
			}
			else
			{
				lightmanager_set_button_lamp(&g_floors[i], DIR_UP, 0);
			}
		}
		// Oppdater bestillingslys nedover utenfor heisen
		if(i > 0)
		{
			if(orderlist_contains(&g_floors[i], DIR_DOWN))
			{
				// Skru på lys på opp knappene utenfor heisen
				lightmanager_set_button_lamp(&g_floors[i], DIR_DOWN, 1);
			}
			else
			{
				lightmanager_set_button_lamp(&g_floors[i], DIR_DOWN, 0);
			}
		}

	}
}

void lightmanager_update_floor_indicators()
{
	floor_t* floor = elevatormanager_get_active_floor_signal();
	if(floor == NULL)
		return;
	// Set indikator lys og last_floor
	g_state.last_floor = floor;
	lightmanager_set_floor_indicator(floor);
}

void lightmanager_update_stop_indicator()
{
	lightmanager_set_stop_lamp(g_state.stop_activated?1:0);
}

void lightmanager_update_door_indicator()
{
	lightmanager_set_door_open_lamp(g_state.door_open?1:0);
}
