/*
 * ordermanager.h
 *
 *  Created on: Feb 26, 2013
 *      Author: janosa
 */

#ifndef ORDERMANAGER_H_
#define ORDERMANAGER_H_

#include "types/floor.h"
#include "types/direction.h"

/**
 * Initialiserer bestillings kontrolleren
 */
void ordermanager_init();

/**
 * Henter nye bestillinger og legger dem inn i ordre listen.
 * Returnerer hvor mange nye bestillinger som ble behandlet.
 */
int ordermanager_fetch_orders();

/**
 * Finner ut om heisen skal stoppe i etasjen hvis vi beveger i retning dir
 */
int ordermanager_should_stop_on_floor(floor_t* floor, direction_t dir);

/**
 * Event som fjerner bestillinger når heisen stopper i en etasje. Dir er en variabel som sier hvilken rettning heisen
 * planlegger å kjøre etterpå.
 *
 * Returnerer hvor mange bestillinger som ble fullført/slettet.
 */
int ordermanager_on_stopped_on_floor(floor_t* floor, direction_t dir);

/**
 * Beregner beste bestilling for å ta hånd om neste
 */
void ordermanager_calculate_target_floor();


#endif /* ORDERMANAGER_H_ */
