/*
 ============================================================================
 Name        : heislab.c
 Author      : Jan Ove Saltvedt
 Version     :
 Copyright   : Copyright Jan Ove Saltvedt og Simen Sollihøgda
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "heislab.h"
#include "unittests.h"

int start_heis()
{
	// Start IO APIet
	if(!elev_init())
	{
		puts("Kunne ikke starte IO APIet!");
		return EXIT_FAILURE;
	}
	puts("Heis Lab av Jan Ove og Simen startet...");
	elevator_init();
	elevator_start();

	char buffer[256];
	while(1)
	{
		puts("Skriv \"stopp\" for å stoppe programmet");
		scanf("%s", buffer);
		if(strcmp(buffer, "stopp") == 0)
		{
			elevator_destroy();
			puts("Avsluttet heis.");
			return EXIT_SUCCESS;
		}
	}

	return EXIT_SUCCESS;
}

int main(void) {
	puts("Velg operasjon:");
	puts("\t1) Kjør heis");
	puts("\t2) Test bestillingsliste.");
	puts("\t3) Lys test.");
	puts("\t4) Heis kontroll sjekk.");
	puts("Skriv inn tall: ");

	int d;
	scanf("%d", &d);
	puts("\n");
	switch(d)
	{
	case 1: return start_heis();
	case 2:
		unittest_init();
		unittest_orderlist();
		return EXIT_SUCCESS;
	case 3:
		unittest_init();
		unittest_lights();
		return EXIT_SUCCESS;
	case 4:
		unittest_init();
		unittest_elevator();
		return EXIT_SUCCESS;
	default:
		puts("Ugyldig operasjon.");
		return EXIT_FAILURE;
	}

	puts("Ugyldig operasjon.");
	return EXIT_FAILURE;
}
