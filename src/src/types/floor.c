/*
 * floor.c
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */

#include "floor.h"

void floor_set_name(floor_t* floor, char* name)
{
	if(floor == 0 || name == 0)
		return;
	if(strlen(name) > 29)
		return;
	memcpy(floor->name, name, strlen(name));
}
