/*
 * state.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef STATE_H_
#define STATE_H_

#include "floor.h"
#include "direction.h"

typedef struct
{
	floor_t* last_floor; // Sist detekterte etasje
	direction_t current_dir; // Hvilken vei beveger vi oss i nå
	direction_t target_dir;
	floor_t* target_floor;
	int stop_activated;
	int door_open;
} state_t;


#endif /* STATE_H_ */
