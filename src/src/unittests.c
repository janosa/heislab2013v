/*
 * unittests.c
 *
 *  Created on: Feb 25, 2013
 *      Author: janosa
 */
#include "unittests.h"
#include "orderlist.h"
#include "lightmanager.h"
#include "elevatormanager.h"
#include <stdlib.h>
#include <stdio.h>
#include "heislab.h"

floor_t floors[4];

int unittest_init()
{
	if(!elev_init())
	{
		puts("Kunne ikke starte IO APIet!");
		return EXIT_FAILURE;
	}
	// Define floors
	floors[0].floor_no = 0;
	floor_set_name(&floors[0], "Ground");

	floors[1].floor_no = 1;
	floor_set_name(&floors[1], "1st");

	floors[2].floor_no = 2;
	floor_set_name(&floors[2], "2nd");

	floors[3].floor_no = 3;
	floor_set_name(&floors[3], "3rd");
	return EXIT_SUCCESS;
}

void unittest_orderlist_listelements()
{
	puts("\tListing orderlist");
	order_t* cur = orderlist_first();
	while(cur != 0)
	{
		printf("\t\tOrder: Floor=%s Direction=%s\n", cur->floor->name, direction_tostring(cur->direction));
		cur = orderlist_next(cur);
	}
}

void unittest_orderlist()
{
	int success = 1;
	puts("Unit test: Order List");
	orderlist_init();
	puts("\tGenerating floors");


	puts("\tAdding order for ground floor DIR=up");
	orderlist_append(&floors[0], DIR_UP);

	puts("\tAdding order for ground floor DIR=nodir");
	orderlist_append(&floors[0], DIR_NODIR);

	puts("\tAdding order for 1st floor DIR=down");
	orderlist_append(&floors[1], DIR_DOWN);

	puts("\tAdding order for 3rd floor DIR=down");
	orderlist_append(&floors[3], DIR_DOWN);

	unittest_orderlist_listelements();

	puts("\tTrying to find an order in Ground floor with DIR=any");
	if(orderlist_contains(&floors[0], DIR_ANY))
	{
		puts("\t\tFOUND! Correct");
		order_t* pOrder = orderlist_get(&floors[0], DIR_ANY);
		printf("\t\t Order is for: floor=%s, dir=%s\n", pOrder->floor->name, direction_tostring(pOrder->direction));
	}
	else
	{
		puts("\t\tNot found! WRONG");
		success = 0;
	}

	puts("\tTrying to find an order in Ground floor with DIR=nodir");
	if(orderlist_contains(&floors[0], DIR_NODIR))
	{
		puts("\t\tFOUND! Correct");
		order_t* pOrder = orderlist_get(&floors[0], DIR_NODIR);
		printf("\t\t Order is for: floor=%s, dir=%s\n", pOrder->floor->name, direction_tostring(pOrder->direction));
	}
	else
	{
		puts("\t\tNot found! WRONG");
		success = 0;
	}

	puts("\tTrying to find an order in Ground floor with DIR=any");
	if(orderlist_contains(&floors[0], DIR_ANY))
	{
		puts("\t\tFOUND! Correct");
	}
	else
	{
		puts("\t\tNot found! WRONG");
		success = 0;
	}

	puts("\tTrying to find an order in 1st floor with DIR=any");
	if(orderlist_contains(&floors[1], DIR_ANY))
	{
		puts("\t\tFOUND! Correct");
	}
	else
	{
		puts("\t\tNot found! WRONG");
		success = 0;
	}

	puts("\tTrying to find an order in 2nd floor with DIR=any, PRI=any");
	if(orderlist_contains(&floors[2], DIR_ANY))
	{
		puts("\t\tFOUND! WRONG");
		success = 0;
	}
	else
	{
		puts("\t\tNot found! Correct");

	}

	unittest_orderlist_listelements();
	puts("\tRemoving order in 1st floor with DIR=any");
	orderlist_remove(&floors[1], DIR_ANY);

	unittest_orderlist_listelements();

	puts("\tTrying to find an order in 1st floor with DIR=any, PRI=any");
	if(orderlist_contains(&floors[1], DIR_ANY))
	{
		puts("\t\tFOUND! WRONG");
		success = 0;
	}
	else
	{
		puts("\t\tNot found! Correct");
	}

	puts("\tClearing order list.");
	if(orderlist_clear() == 3)
	{
		puts("\t\tRemoved 3 elements. Correct");
	}
	else
	{
		puts("\t\tDis not remove 3 elements. WRONG");
		success = 0;
	}

	unittest_orderlist_listelements();

	if(success)
	{
		puts("All tests ran successfully!");
	}
	else
	{
		puts("Test was unsuccessful!!!");
	}
	return;
}

void unittest_lights()
{
	puts("Unit test: Lights");
	lightmanager_init();
	int success = 1;
	int i;
	for(i = 0; i < 4; i++)
	{
		printf("\tTurning on indicator light for floor: %s\n", floors[i].name);
		lightmanager_set_floor_indicator(&floors[i]);
		while(1)
		{
			printf("\t\tIs the light on? [Y/n]");
			char c;
			scanf("%c", &c);
			if(c == 'Y' || c == 'y' || c == '\n')
			{
				puts("\t\tOK.");
				break;
			}
			else if(c == 'N' || c == 'n')
			{
				success = 0;
				puts("\t\tOK.");
				break;
			}
			puts("\t\tInvalid input.");
		}

	}

	for(i = 0; i < 3; i++) // Alle utenom øverste etasje
	{
		printf("\tTurning on the UP button light on floor: %s\n", floors[i].name);
		lightmanager_set_button_lamp(&floors[i], DIR_UP, 1);
		while(1)
		{
			printf("\t\tIs the light on? [Y/n]");
			char c;
			scanf("%c", &c);
			if(c == 'Y' || c == 'y' || c == '\n')
			{
				puts("\t\tOK.");
				break;
			}
			else if(c == 'N' || c == 'n')
			{
				success = 0;
				puts("\t\tOK.");
				break;
			}
			puts("\t\tInvalid input.");
		}
		lightmanager_set_button_lamp(&floors[i], DIR_UP, 0);
	}

	for(i = 1; i < 4; i++) // Alle utenom øverste etasje
	{
		printf("\tTurning on the DOWN button light on floor: %s\n", floors[i].name);
		lightmanager_set_button_lamp(&floors[i], DIR_DOWN, 1);
		while(1)
		{
			printf("\t\tIs the light on? [Y/n]");
			char c;
			scanf("%c", &c);
			if(c == 'Y' || c == 'y' || c == '\n')
			{
				puts("\t\tOK.");
				break;
			}
			else if(c == 'N' || c == 'n')
			{
				success = 0;
				puts("\t\tOK.");
				break;
			}
			puts("\t\tInvalid input.");
		}
		lightmanager_set_button_lamp(&floors[i], DIR_DOWN, 0);
	}

	for(i = 0; i < 4; i++)
	{
		printf("\tTurning on the command button light for floor: %s\n", floors[i].name);
		lightmanager_set_button_lamp(&floors[i], DIR_NODIR, 1);
		while(1)
		{
			printf("\t\tIs the light on? [Y/n]");
			char c;
			scanf("%c", &c);
			if(c == 'Y' || c == 'y' || c == '\n')
			{
				puts("\t\tOK.");
				break;
			}
			else if(c == 'N' || c == 'n')
			{
				success = 0;
				puts("\t\tOK.");
				break;
			}
			puts("\t\tInvalid input.");
		}
		lightmanager_set_button_lamp(&floors[i], DIR_NODIR, 0);
	}

	puts("\tTurning on the open door light");
	lightmanager_set_door_open_lamp(1);
	while(1)
	{
		printf("\t\tIs the light on? [Y/n]");
		char c;
		scanf("%c", &c);
		if(c == 'Y' || c == 'y' || c == '\n')
		{
			puts("\t\tOK.");
			break;
		}
		else if(c == 'N' || c == 'n')
		{
			success = 0;
			puts("\t\tOK.");
			break;
		}
		puts("\t\tInvalid input.");
	}
	lightmanager_set_door_open_lamp(0);

	puts("\tTurning on the stop light");
	lightmanager_set_stop_lamp(1);
	while(1)
	{
		printf("\t\tIs the light on? [Y/n]");
		char c;
		scanf("%c", &c);
		if(c == 'Y' || c == 'y' || c == '\n')
		{
			puts("\t\tOK.");
			break;
		}
		else if(c == 'N' || c == 'n')
		{
			success = 0;
			puts("\t\tOK.");
			break;
		}
		puts("\t\tInvalid input.");
	}
	lightmanager_set_stop_lamp(0);

	if(success)
	{
		puts("All tests ran successfully!");
	}
	else
	{
		puts("Test was unsuccessful!!!");
	}
	return;
}

void unittest_elevator()
{
	puts("Unit test: Elevator control");
	elevatormanager_init();

	puts("\tPlease press the stop button.");
	while(elevatormanager_get_stop_signal() == 0);
	puts("\t\tOK, detected button.");

	puts("\tPlease press the obstruction button.");
	while(elevatormanager_get_obstruction_signal() == 0);
	puts("\t\tOK, detected button.");

	int i;
	for(i = 0; i < 3; i++)
	{
		printf("\tPlease press the up button on floor: %s\n", floors[i].name);
		while(elevatormanager_get_button_signal(&floors[i], DIR_UP)== 0);
		puts("\t\tOK, detected button.");
	}

	for(i = 1; i < 4; i++)
	{
		printf("\tPlease press the down button on floor: %s\n", floors[i].name);
		while(elevatormanager_get_button_signal(&floors[i], DIR_DOWN)== 0);
		puts("\t\tOK, detected button.");
	}

	for(i = 0; i < 4; i++)
	{
		printf("\tPlease press the button for floor %s inside the elevator.\n", floors[i].name);
		while(elevatormanager_get_button_signal(&floors[i], DIR_NODIR)== 0);
		puts("\t\tOK, detected button.");
	}

	puts("\tRunning sensor check.");
	puts("\t\tSetting speed downwards 100");
	elevatormanager_set_speed(-100);
	while(elevatormanager_get_floor_sensor_signal(&floors[0]) == 0);
	elevatormanager_set_speed(0);
	puts("\t\tHit floor 0 sensor.");

	puts("\t\tSetting speed upwards 100");
	elevatormanager_set_speed(100);
	while(elevatormanager_get_floor_sensor_signal(&floors[1]) == 0);
	puts("\t\tHit floor 1 sensor.");
	while(elevatormanager_get_floor_sensor_signal(&floors[2]) == 0);
	puts("\t\tHit floor 2 sensor.");
	while(elevatormanager_get_floor_sensor_signal(&floors[3]) == 0);
	puts("\t\tHit floor 3 sensor.");
	elevatormanager_set_speed(0);

	puts("All tests ran successfully!");
	return;
}
