/*
 * ordermanager.c
 *
 *  Created on: Feb 26, 2013
 *      Author: janosa
 */

#include "ordermanager.h"
#include "heislab.h"

void ordermanager_init()
{

}

int ordermanager_fetch_orders()
{
	int count = 0;
	int i;

	// Hent bestillinger fra inni heisen
	for(i = 0; i < FLOOR_COUNT; i++)
	{
		if(elevatormanager_get_button_signal(&g_floors[i], DIR_NODIR))
		{
			// Knappen er trykket inn.
			if(!orderlist_contains(&g_floors[i], DIR_NODIR))
			{
				// Ordre finnes ikke allerede, legg til
				count += orderlist_append(&g_floors[i], DIR_NODIR); // Legg til ordre
				if(g_state.stop_activated)
				{
					elevator_reinit_after_stop();
				}
			}
		}

		// Bare ta i mot bestillinger fra innsiden når stopp er aktivert
		if(!g_state.stop_activated)
		{
			// Sjekk for bestillinger oppover
			if(i < FLOOR_COUNT-1 && elevatormanager_get_button_signal(&g_floors[i], DIR_UP))
			{
				// Knappen er trykket inn.
				if(!orderlist_contains(&g_floors[i], DIR_UP))
				{
					// Ordre finnes ikke allerede, legg til
					count += orderlist_append(&g_floors[i], DIR_UP); // Legg til ordre
				}
			}

			if(i > 0 && elevatormanager_get_button_signal(&g_floors[i], DIR_DOWN))
			{
				// Knappen er trykket inn.
				if(!orderlist_contains(&g_floors[i], DIR_DOWN))
				{
					// Ordre finnes ikke allerede, legg til
					count += orderlist_append(&g_floors[i], DIR_DOWN); // Legg til ordre
				}
			}
		}
	}
	return count; // Returner anntall endringer i ordrelisten totalt
}

int ordermanager_should_stop_on_floor(floor_t* floor, direction_t dir)
{
	// Det må være en ordre i etasjen for at den skal stoppe.
	if(!orderlist_contains(floor, DIR_ANY))
	{
		return 0;
	}

	if(orderlist_contains(floor, dir) || orderlist_contains(floor, DIR_NODIR))
	{
		return 1;
	}

	// Vi må sjekke om det er ordre videre nedover/oppover som er i den rettningen vi beveger oss i. Hvis ikke stopper vi.
	if(dir == DIR_UP)
	{
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			if(g_floors[i].floor_no <= floor->floor_no)
				continue;
			if(orderlist_contains(&g_floors[i], DIR_UP))
			{
				return 0;
			}
		}
	}
	else if(dir == DIR_DOWN)
	{
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			if(g_floors[i].floor_no >= floor->floor_no)
				continue;
			if(orderlist_contains(&g_floors[i], DIR_DOWN))
			{
				return 0;
			}
		}
	}

	// Si vi går oppover og passerer etasjer som vil nedover. Da vil vi gå til den høyeste etasjen som vil nedover og hoppe over alle på vei oppover som bare vil ned
	if(dir == DIR_UP)
	{
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			if(g_floors[i].floor_no <= floor->floor_no)
				continue;
			if(orderlist_contains(&g_floors[i], DIR_DOWN))
			{
				return 0;
			}
		}
	}
	else if(dir == DIR_DOWN)
	{
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			if(g_floors[i].floor_no >= floor->floor_no)
				continue;
			if(orderlist_contains(&g_floors[i], DIR_UP))
			{
				return 0;
			}
		}
	}
	return 1;
}

int ordermanager_on_stopped_on_floor(floor_t* floor, direction_t dir)
{
	if(floor == NULL || dir == DIR_ANY)
		return 0;

	// Vi har stoppet i en etasje. Vi ønsker da å fjerne alle bestillinger
	// som er gjort fra etasjen i den retningen heisen vil gå etterpå
	// samt alle bestillinger som er gjort til etasjen fra inni heisen.
	int count = 0;
	// Slett alle ordre fra inni heisen
	if(orderlist_contains(floor, DIR_NODIR))
	{
		count += orderlist_remove(floor, DIR_NODIR);
	}

	// Slett alle ordre fra etasjen med samme rettning
	if(orderlist_contains(floor, dir))
	{
		count += orderlist_remove(floor, dir);
	}

	if(!orderlist_has_orders_in_dir(floor, dir))
	{
		count += orderlist_remove(floor, DIR_ANY);
	}
	return count;
}

void ordermanager_calculate_target_floor()
{
	if(orderlist_count() == 0)
	{
		// Ingen bestillinger igjen
		g_state.target_floor = g_state.last_floor;
		g_state.target_dir = DIR_NODIR;
		return;
	}

	direction_t last_target_dir = g_state.target_dir;
	floor_t* next_target_floor = NULL;
	if(last_target_dir == DIR_UP)
	{
		// Hent bestilling ovenfor høyest oppe
		floor_t* best_floor = NULL;
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			floor_t* floor = &g_floors[i];
			if(best_floor != NULL && floor->floor_no <= best_floor->floor_no)
			{
				continue;
			}
			if(orderlist_contains(floor, DIR_ANY))
			{
				best_floor = orderlist_get(floor, DIR_ANY)->floor;
			}
		}
		next_target_floor = best_floor;
	}
	else if(last_target_dir == DIR_DOWN)
	{
		// Hent bestilling nedenfor lengst nede
		floor_t* best_floor = NULL;
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			floor_t* floor = &g_floors[i];
			if(best_floor != NULL && floor->floor_no >= best_floor->floor_no)
			{
				continue;
			}
			if(orderlist_contains(floor, DIR_ANY))
			{
				best_floor = orderlist_get(floor, DIR_ANY)->floor;
			}
		}
		next_target_floor = best_floor;
	}

	if(next_target_floor != NULL)
	{
		g_state.target_floor = next_target_floor;
	}
	else if(last_target_dir == DIR_NODIR)
	{
		// Hent nærmeste bestilling.
		floor_t* best_floor = NULL;
		int best_distance = 9999999;
		int i;
		for(i = 0; i < FLOOR_COUNT; i++)
		{
			floor_t* floor = &g_floors[i];
			if(orderlist_contains(floor, DIR_ANY))
			{
				int distance = g_state.last_floor->floor_no*g_state.last_floor->floor_no + floor->floor_no*floor->floor_no;
				if(distance < best_distance)
				{
					best_floor = orderlist_get(floor, DIR_ANY)->floor;
					best_distance = distance;
				}
			}
		}
		if(best_floor == NULL)
		{
			// This should not happen, but better set it to a safe value.
			g_state.target_floor = g_state.last_floor;
		}
		else
		{
			g_state.target_floor = best_floor;
		}
	}


	// Calculate direction
	if(g_state.target_floor->floor_no > g_state.last_floor->floor_no)
	{
		g_state.target_dir = DIR_UP;
	}
	else if(g_state.target_floor->floor_no == g_state.last_floor->floor_no)
	{
		g_state.target_dir = DIR_NODIR;
	}
	else
	{
		g_state.target_dir = DIR_DOWN;
	}
	return;
}
