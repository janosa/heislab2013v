/*
 * config.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define DOOR_OPEN_TIME_MS 3000

#define FLOOR_COUNT 4

/**
 * Verbosity level
 * 2 - High output <- Sier ifra når bestillinger blir lagt til/slettet
 * 1 - Normal output <- Sier hva heisen skal gjøre etter den når en etasje
 * 0 - Deaktiver output
 */
#define VERBOSE 1

#define ENABLE_QF_FORCE_DOOR
#define ENABLE_QF_STOP

#endif /* CONFIG_H_ */
