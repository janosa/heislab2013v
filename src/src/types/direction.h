/*
 * direction.h
 *
 *  Created on: Feb 13, 2013
 *      Author: janosa
 */

#ifndef DIRECTION_H_
#define DIRECTION_H_

typedef enum
{
	DIR_UP,
	DIR_DOWN,
	DIR_NODIR,
	DIR_ANY = 9000
} direction_t;

char* direction_tostring(direction_t dir);

#endif /* DIRECTION_H_ */
